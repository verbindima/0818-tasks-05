#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <locale.h>

#define MAX 256

int main()
{
	FILE *fin;
	char str[MAX];
	int frequency[MAX];
	int i, onword = 0, numsym = 0, numdig = 0, nummarks = 0, numwords = 0, numletters = 0, average_len;
	int max_in_freq = 0, mostpopularsym = 0;

	setlocale(LC_ALL, "rus");
	fin = fopen("input.txt", "rt");

	for (i = 0; i < MAX; i++)
		frequency[i] = 0;

	while (fgets(str, MAX, fin))
	{
		for (i = 0; i < strlen(str); i++)
		{
			numsym++;

			if (str[i] >= '0' && str[i] <= '9')
				numdig++;

			if (str[i] == '.' || str[i] == ',' || str[i] == '!' || str[i] == '?' || str[i] == ':' || str[i] == ';' || str[i] == '-')
				nummarks++;

			frequency[str[i]]++;

			if (str[i] >= 'a' && str[i] <= 'z' || str[i] >= A' && str[i] <= 'Z')
			{
				if (!onword)
				{
					onword = 1;
					numwords++;
				}
				numletters++;
			}

			if (str[i] == ' ')
				onword = 0;
		}
	}
	
	for (i = 0; i < MAX; i++)
		if (frequency[i] > max_in_freq)
		{
			max_in_freq = frequency[i];
			mostpopularsym = i;
		}

	printf("���������� �������� � �����:\n%d\n", numsym);
	printf("���������� ���� � �����:\n%d\n", numwords);
	printf("���������� ������ ���������� � �����:\n%d\n", nummarks);
	printf("���������� ���� � �����:\n%d\n", numdig);
	printf("������� ����� ����� � �����:\n%d\n", numletters / numwords);
	printf("����� ���������� ������ � �����:\n'%c'\n", mostpopularsym);
	fclose(fin);

	return 0;
}
