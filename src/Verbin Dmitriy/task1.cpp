#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define size 90

struct BOOK
{
	char namebook[50];
	char nameauthor[30];
	int year;
};

void getbook(struct BOOK *p, char *str)
{
	int i,j,k;
	char buf[50];
	k = i = 0;
	while(k < 3)
	{
		j = 0;
		while((str[i] != ';')&&(str[i] != '\n'))
		{
			buf[j] = str [i];
			i++; j++;
		}
		buf[j] = 0;
		switch(k){
			case 0:
				strcpy(p -> namebook, buf);
			break;
			case 1:
				strcpy(p -> nameauthor, buf);
			break;
			case 2:
				p -> year = atoi(buf);
				break;
		}
		i++; k++;
	}
}
void authsort(struct BOOK *cat, int *marks, int N)
{
	int buf;
	int i, j;
	for(i = 1; i < N; i++)
	{
		buf = marks[i];
		j = i - 1;
		while((j >= 0)&&(strcmp(cat[marks[j]].nameauthor, cat[buf].nameauthor) > 0))
		{
			marks[j+1] = marks[j];
			j--;
		}
		marks[j+1] = buf;
	}
}

int main()
{
	FILE *fin;
	struct BOOK *catalog;
	int *marks;
	int i = 0, N = 0, maxy, miny;
	char str[size];
	
	fin = fopen("input1.txt", "rt");
	while (fgets(str, 128, fin))
		N++; 
	rewind(fin);
	catalog = (struct BOOK*)malloc(N*sizeof(struct BOOK));
	marks = (int*)malloc(N*sizeof(int));
	if(catalog > 0)
	{
		i = 0;
		while (fgets(str, 128, fin))
		{
		getbook(&(catalog[i]), str);
			i++;
		}	
		maxy = 0;
		miny = 3000;
		for(i = 0; i < N; i++)
		{
			if(catalog[i].year > maxy)
				maxy = catalog[i].year;
			if(catalog[i].year < miny)
				miny = catalog[i].year; 
			printf("%-20s| %-16s| %4d\n", catalog[i].namebook, catalog[i].nameauthor, catalog[i].year);
		}
		puts("");
		printf("MaxYear: %d MinYear: %d\n", maxy, miny);
		for(i = 0; i < N; marks[i] = i++);
		authsort(catalog, marks, N);
		puts(" ");
		for(i = 0; i < N; i++)
			printf("%-16s| %-20s| %4d\n", catalog[marks[i]].namebook, catalog[marks[i]].nameauthor,  catalog[marks[i]].year);
		free(catalog);
		free(marks);
		return 0;
	}
	return 1;
}
